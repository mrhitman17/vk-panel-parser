var express = require('express');
var mysql   = require('mysql');
var fs      = require('fs');
var needle  = require('needle');
var cheerio = require('cheerio');

const zlib = require('zlib');
const gzip = zlib.createGzip();

var config = require('./config.json');

const request = require('request');
//Подключение к базе данных
var db = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database,
    port: config.db.port
});
//Подключаемся к базе данных
db.connect();
var app = express();
var VP = {
    //Перенос удаленных записей через 6 часов после удаления в архив
    transferWallPostFroArhive: function(){
        /*db.query("SELECT * FROM `groups_to_wall` WHERE status = '1' AND date_remove > NOW() - INTERVAL 18000 SECOND", function(error, rows){
            if(error) return console.log(error);
            if(rows.length > 0){
                for(var id in rows){
                    db.query("INSERT INTO `groups_to_wall_archive` (`type`, `type_remove`, `group_id`, `vk_group_id`, `vk_post_id`, `vk_created_by`, `vk_date`, `vk_remove`, `date_add`, `date_update`, `date_remove`, `status`) SELECT `type`, `type_remove`, `group_id`, `vk_group_id`, `vk_post_id`, `vk_created_by`, `vk_date`, `vk_remove`, `date_add`, `date_update`, `date_remove`, `status` FROM `groups_to_wall` WHERE groups_to_wall_id = '"+rows[id]['groups_to_wall_id']+"' LIMIT 1", function(error, query){
                        db.query("DELETE FROM `groups_to_wall` WHERE groups_to_wall_id = '"+rows[id]['groups_to_wall_id']+"'");
                    });
                }
            }
        });*/
    },
    startStopGroup: function(){
        db.query("SELECT users_id, status FROM `NodeTaskMeneger` WHERE status = '3' AND errorCode = '29' AND date_add + INTERVAL 600 SECOND < NOW() GROUP BY users_id", function(error, rows){
            if(rows.length > 0){
                for(var id in rows){
                    db.query("UPDATE `groups` SET status = '1' WHERE status = '5' AND users_id = '"+rows[id]['users_id']+"'");
                    db.query("DELETE FROM `NodeTaskMeneger` WHERE users_id = '"+rows[id]['users_id']+"' AND status = '3'");
                }
            }
        });
    },
    //парсинг стены группы для превью и сжатие в архив
    loadPreviewZip: function(group_id, post_id){
        needle.get('https://vk.com/wall' + group_id + '_' + post_id, function(error, res){
            if(!error){
                var $ = cheerio.load(res.body);
                $(".rel_date").html("{{replace_date}}");
                var compress = zlib.createGzip(), 
                    input    = $("#wide_column").html();
                fs.mkdir(config.srcPreViewPostForGroup + group_id, function(){    
                    fs.appendFile(config.srcPreViewPostForGroup + group_id + '/'+post_id+'.txt', input + '\r\n', 'utf8', (err) => {
                      if (err) throw err;
                      
                      var gzip = zlib.createGzip();
                      var inp = fs.createReadStream(config.srcPreViewPostForGroup + group_id + '/' +post_id + '.txt');
                      var out = fs.createWriteStream(config.srcPreViewPostForGroup + group_id + '/' + post_id + '.txt.gz');

                      var stream = inp.pipe(compress).pipe(out);
                      
                      stream.on('finish', function () { 
                        fs.unlink(config.srcPreViewPostForGroup + group_id + '/' +post_id + '.txt', function(err){
                            if(err) return console.log(err);
                            console.log('Превью для записи ' + post_id + ' в группе, успешно сохранено');
                        });
                      });
                      
                    });
                });
            }else{
                VP.loadPreviewZip(group_id, post_id);
            }
        });
    }
};

//Включение продления подписок
function renewalAccoun(){
    db.query("SELECT * FROM users u LEFT JOIN users_group ug ON ug.users_group_id = u.users_group_id WHERE u.date_service_end <= NOW()", function(error, query){
        if(error) throw error;
        if(query.length > 0){
            for(var i = 0; i < query.length; i++){
                var account = query[i];
                var storage = JSON.parse(account['storage']);
                if(storage['service']['type'] == 1){
                    if(account['balance'] >= storage['service']['amount']){
                        db.query("UPDATE users SET date_service_start = NOW(), date_service_end = DATE_ADD(NOW(), INTERVAL 30 DAY), balance = balance - "+parseFloat(storage['service']['amount'])+" WHERE users_id = '"+account['users_id']+"'");
                        db.query("INSERT INTO `users_balance_history`(`users_id`, `description`, `amount`, `date_add`, `type`, `status`) VALUES ('"+account['users_id']+"', 'Автоматическое продление подписки', '"+parseFloat(storage['service']['amount'])+"', NOW(), 'debit', '1')");
                        db.query("UPDATE `groups` SET status = '1', date_payment = DATE_ADD(NOW(), INTERVAL 30 DAY) WHERE users_id = '"+account['users_id']+"' AND status != '4'");
                    }else{
                        db.query("UPDATE `groups` SET status = '3' WHERE users_id = '"+account['users_id']+"' AND status != '4'");
                    }
                }
            }
        }
        
    });
}
function autoAddTaskCheckTaskByUser(user){
    db.query("SELECT COUNT(*) as total, (SELECT group_id FROM `NodeTaskMeneger` WHERE `status` = '1' AND `users_id` = '" + user['users_id'] + "' ORDER BY `NodeTaskMeneger_id` DESC LIMIT 1) as group_id_last FROM `NodeTaskMeneger` WHERE `status` = '0' AND `users_id` = '" + user['users_id'] + "'", function(error, task){
        if(task[0]['total'] == 0){
            db.query("SELECT * FROM `groups` WHERE `users_id` = '" + user['users_id'] + "' AND `status` = '1' AND `date_payment` >= NOW()", function(error, groups){
                if(groups.length > 0){
                    var groupKey = 0;
                    if(task[0]['group_id_last'] != null){
                        for(var groupIndex in groups){
                            var group = groups[groupIndex];
                            if(group['id'] == task[0]['group_id_last']){
                                groupKey = parseInt(groupIndex) + 1;
                                break;
                            }
                       }
                    }
                    if (typeof groups[groupKey] == 'undefined') {
                        groupKey = 0;
                    }
                    db.query("SELECT COUNT(*) as total FROM `NodeTaskMeneger` WHERE group_id = '" + groups[groupKey]['id'] + "' AND status = '0'", function(error, result){
                        if(result[0]['total'] == 0){
                            db.query("INSERT INTO `NodeTaskMeneger`(`group_id`, `vk_group_id`, `users_id`, `status`, `storageTask`, `errorCode`, `date_add`) VALUES ('"+groups[groupKey]['id']+"', '"+groups[groupKey]['gid']+"', '"+groups[groupKey]['users_id']+"', '0', '', '', NOW())");
                            console.log('Добавил новую группу в список задач: ' + groups[groupKey]['id'] + ' Для пользователя' + user['users_id']);
                        }else{
                            console.log('Группа в ожидание выполнения');
                        }
                    });
                }
            });
        }else{
            //console.log('Превышен лимит групп у пользователя: '+ user['users_id']);
        }
    });
}
function autoAddTaskUsers(error, users){
    if(users.length > 0){
        for(var key in users){
            var user = users[key];
            //Проверяем пользователя на задачи
            autoAddTaskCheckTaskByUser(user);
        }
    }else{
        console.log('Доступных пользователей нет.');
    }
}
/*
 * Менеджер задача
 *
 * Статусы typeStatus (0 => Ожидает запуска, 1 => Задача выполнена, 2 => Ожидает выполнения, 3 => Ошибка)
*/
var applicationTaskMeneger = {
    //Автоматическое добавление задач
    'autoAddTask': function(){
        db.query("SELECT u.users_id, us.access_token, GROUP_CONCAT(s.code) as serviceList, u.email FROM `users` u LEFT JOIN `users_social` us ON us.users_id = u.users_id LEFT JOIN service_to_users sts ON sts.users_id = u.users_id LEFT JOIN service s ON s.service_id = sts.service_id WHERE u.users_id IN(SELECT users_id FROM `groups` WHERE status = '1' AND date_payment >= NOW() AND us.type = 'account' GROUP BY users_id)GROUP BY u.users_id", autoAddTaskUsers);
        //Ожидаем 2 секунды и перезапускаем задачи
        console.log('Автоматическое добавление задач');
        //Автоматическое продление подписки
        renewalAccoun();
        //Перенос удаленных записей в архив
        VP.transferWallPostFroArhive();
        //Автозапуск приостановленных групп
        VP.startStopGroup();
        //Очистка выполненых задач
        db.query("SELECT NodeTaskMeneger_id as id FROM `NodeTaskMeneger` WHERE (date_add < NOW() - INTERVAL 900 SECOND AND status = '1') OR group_id IN(SELECT `id` FROM groups WHERE `status` = 4)", function(error, query){
            if(query.length > 0){
                for(row in query){
                    db.query("DELETE FROM `NodeTaskMeneger` WHERE NodeTaskMeneger_id = '"+query[row]['id']+"'");
                    db.query("DELETE FROM `NodeTaskMeneger_to_status` WHERE NodeTaskMeneger_id = '"+query[row]['id']+"'");
                }
            }
        });
    },
    /*
     * Коды ошибок:
     * s1 => Ответ от вк не был получен
     * s2 => Нет прав на группу у текущего пользователя
    */
    'errorTask': function(taskId, errorCode, comment){
        
        var errorDescription = errorCode;
        
        if(errorCode == 29){
            errorDescription = 'Ошибка №29<br/>Превышен лимит запросов с аккаунта (Не более 5000 запросов стены за 24 часа) <br /> Рекомендуем подключить новый аккаунт VK';
        }
        if(errorCode == 5){
            errorDescription = 'Ошибка №5<br/>Авторизация пользователя не удалась, переподключите аккаунт VK';
        }
        if(errorCode == 's2'){
            errorDescription = 'У одной или нескольких групп пропали права доступа';
        }
        
        db.query("UPDATE `NodeTaskMeneger` SET `storageTask` = '" + errorDescription + "', `status` = '3', `errorCode` = '" + errorCode + "' WHERE `NodeTaskMeneger_id` = '" + taskId + "'");
        
        if(errorCode == 's1'){
            return 0;
        }
        
        db.query("SELECT users_id FROM `NodeTaskMeneger` WHERE `NodeTaskMeneger_id` = '" + taskId + "'", function(error, task){
            if(task.length > 0){
                db.query("INSERT INTO `users_notification`(`users_id`, `description`, `status`, `type`, `date_add`) VALUES ('"+task[0]['users_id']+"', '"+errorDescription+"', '0', 'danger', NOW())");
                
                //Приостанавливаем группу из-за ошибки 29
                if(errorCode == 29){
                    db.query("UPDATE `groups` SET status = '5' WHERE status = '1' AND users_id = '"+task[0]['users_id']+"'");
                }
                if(errorCode == 5){
                    db.query("UPDATE `groups` SET status = '0' WHERE status = '1' AND users_id = '"+task[0]['users_id']+"'");
                }
            }
        });
    }, 
    'successTaskById': function(taskId){
        console.log('Завершение задачи №' + taskId);
        db.query("UPDATE `NodeTaskMeneger` SET `status` = '1' WHERE `NodeTaskMeneger_id` = '" + taskId + "'");
    },
    //Успешно выполненная задача
    'successTask': function(taskId, status){
        db.query("INSERT INTO `NodeTaskMeneger_to_status`(`NodeTaskMeneger_id`, `NodeTaskMenegerStatus_id`, `date_add`) VALUES ('"+taskId+"', '"+status+"', NOW())");
    },
    'checkGroup': function(data){
        //Автоматическое обновление данных группы
        request('https://api.vk.com/method/groups.getById?group_id=' + data['vk_group_id'] + '&access_token=' + data['access_token'] + '&fields=id,name,photo_200,admin_level,is_admin&v=5.78', function(error, response, body){
            if (typeof response !== "undefined" && response.statusCode == 200) {
                var dataGroup = JSON.parse(body);
                if(isset("response", dataGroup)){
                    //Обновляем группу
                    var groupStatus = 1;
                    
                    if(dataGroup['response'][0]['is_admin'] == 0){groupStatus = 0;}
                    if(dataGroup['response'][0]['admin_level'] == 1){ groupStatus = 0;}
                    
                    db.query("UPDATE `groups` SET photo = ?, `status` = ?, date_check = NOW() WHERE id = ?", [
                        dataGroup['response'][0]['photo_200'],
                        groupStatus,
                        data['group_id']
                    ]);
                    
                    //Обновляем текущую задачу
                    if(groupStatus == 1){
                        //Отправляем на проверку группы
                        wallGet(data['NodeTaskMeneger_id'], {
                            "token"           : data['access_token'],
                            "id"              : data['group_id'],
                            "gid"             : data['vk_group_id'],
                            "serviceList"     : data['serviceList'],
                            "view_post_before": data['view_post_before'],
                            "time"            : data['time'],
                            "coverage"        : data['coverage'],
                            "is_pinned"       : data['is_pinned'],
                            "gc_status"       : data['gc_status'],
                        });
                        applicationTaskMeneger.successTask(data['NodeTaskMeneger_id'], 1);
                    }else{
                        applicationTaskMeneger.errorTask(data['NodeTaskMeneger_id'], 's2', 'checkGroup');
                    }
                }else{
                    var errorCode = dataGroup['error']['error_code'];
                    applicationTaskMeneger.errorTask(data['NodeTaskMeneger_id'], errorCode, 'checkGroup');
                }
            }else{
                applicationTaskMeneger.errorTask(data['NodeTaskMeneger_id'], 's1', 'checkGroup');
            }
        });
    },
    'TaskStart': function(){
        db.query("SELECT ntm.group_id, ntm.vk_group_id, ntm.NodeTaskMeneger_id, us.access_token, g.is_pinned, g.view_post_before, ntm.users_id, GROUP_CONCAT(s.code) as serviceList, gc.time, gc.coverage, gc.status as gc_status FROM NodeTaskMeneger ntm LEFT JOIN groups g ON g.id = ntm.group_id LEFT JOIN groups_coverage gc ON gc.group_id = g.id LEFT JOIN users_social us ON us.users_id = ntm.users_id LEFT JOIN service_to_users sts ON sts.users_id = ntm.users_id LEFT JOIN service s ON s.service_id = sts.service_id WHERE ntm.status = '0' AND g.status = '1' AND us.type = 'account' GROUP BY ntm.users_id", function(error, rows){
            if(error) throw error;
            if(rows.length > 0){
                for(var i = 0; i < rows.length; i++){
                    var row        = rows[i];
                    row.serviceList = row.serviceList.split(','); 
                    applicationTaskMeneger.checkGroup(row);
                }
            }else{
                console.log('Нет доступных задач');
            }
        });
    }
};
app.get('/applicationTaskMenegerExecute', function(request, response){
    
    applicationTaskMeneger.autoAddTask();
    
    setTimeout(function(){
        applicationTaskMeneger.TaskStart();
    }, 5000);

    response.send('Менеджер задач успешно запущен!');
});
var application = {
    "clearnWallByGroup": function(taskId, group){
        //Запускаю очистку группы
        setTimeout(function(){
            RemoveGroupWallPost(taskId, group);
        }, 20000);
    },
    "getQuery": function(domain){
        request(domain);
    }
}
app.listen(config.application.port, function() {
    console.log('start application');
});
function sleep(ms) {
  ms += new Date().getTime();
  while (new Date() < ms){}
}
function escape(string){
    if(string !== null && empty(string) == false){
        string.replace("'", "\'");
        string.replace('"', '\"');
    }
    return string;
}



function addLogByGroup(group_id, text){
    
    return false;
    
    /*var date = new Date();
    var dir = date.getDate() + '.' + date.getMonth() + '.' + date.getFullYear();
    
    text = date.getHours() + ':' + date.getMinutes() + ':'+ date.getSeconds() + ' | ' + text;
    
    fs.access("./log/"+group_id, function(error){
        if (error) {
            fs.mkdir('./log/'+group_id, function(){
                fs.appendFile('./log/'+group_id+'/'+dir+'.txt', text + '\r\n', 'utf8', (err) => {
                  if (err) throw err;
                });
            });
        }else{
            fs.appendFile('./log/'+group_id+'/'+dir+'.txt', text + '\r\n', 'utf8', (err) => {
              if (err) throw err;
            });
        }
    });*/
}
//Удаление записей по рекламным фильтрам
function RemoveGroupWallPostFilter(group, data) {
    console.log('В группе №' + group['id'] + ', записей на удаление по фильтрам: '+data.length);
    if(data.length > 0){
        console.log('Начинаю удалять по фильтрам');
        var codeExecuteDelete = '';
        var codeExecute = '';
        for(var row in data){
            codeExecuteDelete += '"' + row + '": API.wall.delete({"owner_id": "' + data[row]['vk_group_id'] + '", "post_id": "' + data[row]['vk_post_id'] + '"}), ';
        }
        codeExecute = 'return {"stat":{' + codeExecuteDelete + '}};';
        request('https://api.vk.com/method/execute?code=' + codeExecute + '&access_token=' + group['token'] + '&v=3.0', function(error, response, body) {
            if (typeof response !== 'undefined' && response.statusCode == 200){
                var result = JSON.parse(body);
                if (isset("response", result)) {
                    console.log(result);
                    for(var row in result['response']['stat']){
                        if(result['response']['stat'][row] > 0){
                           //Запись удалена 
                           db.query("UPDATE `groups_to_wall` SET status = '1', date_remove = NOW(), type_remove = 'filter' WHERE vk_group_id = '" + data[row]['vk_group_id'] + "' AND vk_post_id = '" + data[row]['vk_post_id'] + "'");
                           addLogByGroup("-" + group['gid'], 'Удалил из группы: ' + data[row]['vk_group_id'] + ' запись ' + data[row]['vk_post_id']);
                        }else{
                            console.log('Не могу удалить запись: ' + data[row]['vk_post_id'] + ', группа: ' + data[row]['vk_group_id']);
                        }
                    }
                }else{
                    console.log(result);
                }
            }
        });
    }
}
function RemoveGroupWallPostCoverage(group, data) {
    console.log('В группе №' + group['id'] + ', записей на удаление по охвату: '+data.length);
    addLogByGroup('-' + group['gid'], 'Нашел записей на удаление по охвату:' + data.length);
    if(data.length > 0){
        console.log('Начинаю удалять по фильтрам');
        var codeExecuteDelete = '';
        var codeExecute = '';
        for(var row in data){
            codeExecuteDelete += '"' + row + '": API.wall.delete({"owner_id": "' + data[row]['vk_group_id'] + '", "post_id": "' + data[row]['vk_post_id'] + '"}), ';
        }
        codeExecute = 'return {"stat":{' + codeExecuteDelete + '}};';
        request('https://api.vk.com/method/execute?code=' + codeExecute + '&access_token=' + group['token'] + '&v=3.0', function(error, response, body) {
            if (typeof response !== 'undefined' && response.statusCode == 200){
                var result = JSON.parse(body);
                if (isset("response", result)) {
                    for(var row in result['response']['stat']){
                        if(result['response']['stat'][row] > 0){
                           //Запись удалена 
                           db.query("UPDATE `groups_to_wall` SET status = '1', date_remove = NOW(), type_remove = 'coverage' WHERE vk_group_id = '" + data[row]['vk_group_id'] + "' AND vk_post_id = '" + data[row]['vk_post_id'] + "'");
                           //Перечисляем причины удаления
                           if(data[row]['score'].length > 0){
                               for( var sc = 0; sc < data[row]['score'].length; sc ++){
                                   db.query("INSERT INTO `groups_to_wall_score`(`vk_group_id`, `vk_post_id`, `score`, `description`) VALUES ('" + data[row]['vk_group_id'] + "', '" + data[row]['vk_post_id'] + "', '" + data[row]['score'][sc] + "', '"+data[row]['score_description'][sc]+"')");
                               }
                           }
                           addLogByGroup("-" + group['gid'], 'Удалил из группы: ' + data[row]['vk_group_id'] + ' запись ' + data[row]['vk_post_id']);
                           console.log('Удалил запись: ' + data[row]['vk_post_id'] + ', группа: ' + data[row]['vk_group_id']);
                        }else{
                            console.log('Не могу удалить запись: ' + data[row]['vk_post_id'] + ', группа: ' + data[row]['vk_group_id']);
                        }
                    }
                }else{
                    console.log(result);
                }
            }
        });
    }

}
function RemoveGroupWallPost(taskId, group) {
  var time = Math.round(new Date().getTime() / 1000);
  //console.log('Собираю данные по записям для удаления: ' + group['id']);
  db.query("SELECT wg.vk_date, wg.vk_remove, gtws.vk_coverage, gc.coverage as gc_vk_coverage, wg.group_id, wg.vk_post_id, wg.vk_group_id, wg.status, gc.time, wg.vk_date, wg.type,  gc.status as gc_status, gc.vk_likes as gc_vk_likes, gtws.vk_likes, gc.vk_reposts as gc_vk_reposts, gtws.vk_reposts, gc.vk_reach_viral as gc_vk_reach_viral, gtws.vk_reach_viral, gc.vk_reach_ads as gc_vk_reach_ads, gtws.vk_reach_ads, gc.vk_reach_total as gc_vk_reach_total, gtws.vk_reach_total, gc.vk_links as gc_vk_links, gtws.vk_links, gc.vk_to_group as gc_vk_to_group, gtws.vk_to_group, gc.vk_join_group as gc_vk_join_group, gtws.vk_join_group, gc.vk_report as gc_vk_report, gtws.vk_report, gc.vk_hide as gc_vk_hide, gtws.vk_hide, gc.vk_comments as gc_vk_comments, gtws.vk_comments FROM `groups_to_wall` wg LEFT JOIN groups_coverage gc ON gc.group_id = wg.group_id LEFT JOIN groups_to_wall_stat gtws ON gtws.vk_group_id = wg.vk_group_id WHERE gtws.vk_post_id = wg.vk_post_id AND wg.group_id = '" + group['id'] + "' AND wg.vk_remove <= '" + time + "' AND wg.status = '0' LIMIT 25", function(error, res){
    if (error) throw error;
    
    //Завершение подзадачи 789
    applicationTaskMeneger.successTask(taskId, 3);
    //Завершение текущей задачи
    applicationTaskMeneger.successTaskById(taskId);
    
    if (res.length > 0) {
      var storagePostFilter = [];
      //Хранение стены по охвату
      var storagePostCoverage = [];
      //перебор массива
      for (var i = 0; i < res.length; i++) {
          
          if(res[i]['type'] == ''){
              //Если услуга у пользователя есть и функция охвата включена
              if (in_array('coverage', group['serviceList']) == true && res[i]['gc_status'] == 1){
                  var score = [];
                  var score_description = [];
                  //Удаление по комментариям
                  if(res[i]['gc_vk_comments'] != -1 &&  res[i]['gc_vk_comments'] > res[i]['vk_comments']){
                      score.push('comments');
                      score_description.push(res[i]['vk_comments'] +'  < ' + res[i]['gc_vk_comments']);
                  }
                  //если лайков меньше на записи чем указано
                  if(res[i]['gc_vk_likes'] != -1 &&  res[i]['gc_vk_likes'] > res[i]['vk_likes']){
                      score.push('likes');
                      score_description.push(res[i]['vk_likes'] +'  < ' + res[i]['gc_vk_likes']);
                  }
                  //Если репосты включены, и на записи репостов меньше чем в настройках
                  if(res[i]['gc_vk_reposts'] != -1 && res[i]['gc_vk_reposts'] > res[i]['vk_reposts']){
                      score.push('reposts');
                      score_description.push(res[i]['vk_reposts'] +' < ' + res[i]['gc_vk_reposts']);
                  }
                  //Охват
                  if(res[i]['gc_vk_coverage'] != -1 && res[i]['gc_vk_coverage'] > res[i]['vk_coverage']){
                      score.push('coverage');
                      score_description.push(res[i]['vk_coverage'] +' < ' + res[i]['gc_vk_coverage']);
                  }
                  //Удалять контент записи с суммарным охватом менее
                  if(res[i]['gc_vk_reach_total'] != -1 && res[i]['gc_vk_reach_total'] > res[i]['vk_reach_total']){
                      score.push('reach_total');
                      score_description.push(res[i]['vk_reach_total'] +' < ' + res[i]['gc_vk_reach_total']);
                  }
                  //Удалять контент записи с рекламным охватом менее
                  if(res[i]['gc_vk_reach_ads'] != -1 && res[i]['gc_vk_reach_ads'] > res[i]['vk_reach_ads']){
                      score.push('reach_ads');
                      score_description.push(res[i]['vk_reach_ads'] +' < ' + res[i]['gc_vk_reach_ads']);
                  }
                  //Удалять контент записи с виртуальным охватом менее
                  if(res[i]['gc_vk_reach_viral'] != -1 && res[i]['gc_vk_reach_viral'] > res[i]['vk_reach_viral']){
                      score.push('reach_viral');
                      score_description.push(res[i]['vk_reach_viral'] +' < ' + res[i]['gc_vk_reach_viral']);
                  }
                  //Удалять контент записи с переходами по ссылке менее
                  if(res[i]['gc_vk_links'] != -1 && res[i]['gc_vk_links'] > res[i]['vk_links']){
                      score.push('links');
                      score_description.push(res[i]['vk_links'] +' < ' + res[i]['gc_vk_links']);
                  }
                  //Удалять контент записи с переходами в сообщество менее
                  if(res[i]['gc_vk_to_group'] != -1 && res[i]['gc_vk_to_group'] > res[i]['vk_to_group']){
                      score.push('to_group');
                      score_description.push(res[i]['vk_to_group'] + ' < ' + res[i]['gc_vk_to_group']);
                  }
                  //Удалять контент записи с вступлениями в сообщество менее
                  if(res[i]['gc_vk_join_group'] != -1 && res[i]['gc_vk_join_group'] > res[i]['vk_join_group']){
                      score.push('join_group');
                      score_description.push(res[i]['vk_join_group'] +' < ' + res[i]['gc_vk_join_group']);
                  }
                  //Удалять контент записи с количеством отписавшихся участников более
                  if(res[i]['gc_vk_unsubscribe'] != -1 && res[i]['gc_vk_unsubscribe'] < res[i]['vk_unsubscribe']){
                      score.push('unsubscribe');
                      score_description.push(res[i]['vk_unsubscribe'] +' > ' + res[i]['gc_vk_unsubscribe']);
                  }
                  //Удалять контент записи с количеством жалоб на запись более
                  if(res[i]['gc_vk_report'] != -1 && res[i]['gc_vk_report'] < res[i]['vk_report']){
                      score.push('report');
                      score_description.push(res[i]['vk_report'] +' > ' + res[i]['gc_vk_report']);
                  }
                  //Удалять контент записи с количеством скрывших запись более
                  if(res[i]['gc_vk_hide'] != -1 && res[i]['gc_vk_hide'] < res[i]['vk_hide']){
                      score.push('hide');
                      score_description.push(res[i]['vk_hide'] +' > ' + res[i]['gc_vk_hide']);
                  }
                  //Если правила на удаление найдены тогда удаляем запись
                  if(score.length > 0){
                      res[i]['score'] = score;
                      res[i]['score_description'] = score_description;
                      storagePostCoverage.push(res[i]);
                  }else{
                      //Правил не найдено, пост делаем контентом
                      db.query("UPDATE `groups_to_wall` SET status = '5', type = 'post' WHERE vk_post_id = '" + res[i]['vk_post_id'] + "' AND vk_group_id = '" + res[i]['vk_group_id'] + "'");
                      continue;
                  }                  
                  
              }else{
                  //Если функция охвата выключена, определяем запись как пост
                  db.query("UPDATE `groups_to_wall` SET status = '5', type = 'post' WHERE vk_post_id = '" + res[i]['vk_post_id'] + "' AND vk_group_id = '" + res[i]['vk_group_id'] + "'");
                  continue;
              }
          }
          //Если тип чека определен отправляем на рекламу
          if (res[i]['type'] != '' && in_array('filter', group['serviceList']) == true) {
                storagePostFilter.push(res[i]);
          }
      }
      //Запускаем удаление по охвату
      RemoveGroupWallPostCoverage(group, storagePostCoverage);
      //Запускаем удаление по фильтрам
      RemoveGroupWallPostFilter(group, storagePostFilter);
    }else{
        //console.log('В группе №' + group['id'] + ' нет записей на удаление');
    }
  });
}
function addWallForRemove(data){
 	var vk_remove = data['date_wall'] + (data['time'] * 60);
    var field_status    = "";
    
    if(data['is_not_remove'] == 1){
        vk_remove = data['date_wall']; 
        field_status = ", status = '2'";
    }
	db.query("UPDATE `groups_to_wall` SET type = '"+data['type']+"', vk_remove = '"+vk_remove+"' "+field_status+" WHERE vk_post_id = '"+data['post_id']+"' AND vk_group_id = '"+data['gid']+"'");
}
/*
 *
 * Функция для поиска значений по фильтрам пользователей (Массовая)
 *
*/
function getFilterTypeMass(group, wall, type, filter, string){
	db.query("SELECT * FROM groups_to_mass_filter gtmf LEFT JOIN mass_filter mf ON mf.mass_filter_id = gtmf.mass_filter_id WHERE gtmf.groups_id = '"+group['id']+"' AND mf.type = '"+type+"'", function(error, rows){
		if(rows.length > 0){

			var resultCheck = 0;
			for(var i = 0; i < rows.length; i++){
				var fil = rows[i];
				if(string.indexOf(fil['filter']) + 1){

					addLogByGroup("-"+group['gid'], 'Массовый фильтр нашел в записи фильтр:' + fil['filter'] + '| запись ='+ wall['id']);

					addWallForRemove({
						"group_id"	: group['id'],
						"post_id"   : wall['id'],
						"gid"		: wall['owner_id'],
						"date_wall" : wall['date'],
						"time"      : fil['time'],
                        "is_not_remove": fil['is_not_remove'],
						"type"		: 'specific_filter'
					});

					resultCheck = 1;
					break;
				}
			}

			if(resultCheck == 0 && type == 0){

				addLogByGroup("-"+group['gid'], 'Массовый фильтр не нашел записи применяю только общий | запись ='+ wall['id']);
								
				addWallForRemove({
					"id"	: group['id'],
					"post_id"   : wall['id'],
					"gid"		: wall['owner_id'],
					"date_wall" : wall['date'],
					"time"      : filter['time'],
                    "is_not_remove": filter['is_not_remove'],
					"type"		: 'general_filter',
				});
			}

		}else{
			if(type == 0){
				addLogByGroup("-"+group['gid'], 'Массовый фильтр у группы не задан, применяю только общий фильтр');
				addWallForRemove({
					"id"	: group['id'],
					"post_id"   : wall['id'],
					"gid"		: wall['owner_id'],
					"date_wall" : wall['date'],
					"time"      : filter['time'],
                    "is_not_remove": filter['is_not_remove'],
					"type"		: 'general_filter',
				});
			}
		}
	});
}
/*
 *
 * Функция для поиска значения по фильтрам пользователям (Общая)
 *
*/
function getFilterTypeGeneral(group, wall, type, string){
	db.query("SELECT * FROM `groups_filter` where `fid`='"+group['id']+"' AND `type`='1' LIMIT 1", function(error, row){
        if(row.length > 0){
        	addLogByGroup("-"+group['gid'], 'Проверка записи по общему фильтру №'+wall['id']);
			var filter           = row[0]['filtre_name'].split(',');
            var filter_exception = row[0]['exception'].split(',');
            var is_copy_history  = row[0]['is_copy_history'];
            
            if(filter_exception.length > 0){
                for(var i = 0; i < filter_exception.length; i++){
                    var exception = filter_exception[i];
                    var is_exception = 0;
                    if(empty(exception) == true){
						continue;
					}
                    if(string.indexOf(exception) + 1){
                        addLogByGroup("-"+group['gid'], 'Нашел исключение "'+exception+'" в записи '+wall['id']);
                        
                        if(group['gc_status'] == 0){
                            db.query("UPDATE `groups_to_wall` SET status = '5', type = 'post' WHERE vk_post_id = '"+wall['id']+"' AND vk_group_id = '"+wall['owner_id']+"'");
                        }
                        
                        is_exception = 1;
                        break; 
                    }
                }
                if(is_exception == 1){
                    return true;
                }
            }

			if(row[0]['is_marked_as_ads'] == 0 && wall['marked_as_ads'] == 1){
				addLogByGroup(wall['owner_id'], 'Общий фильтр, запись с рекламной меткой изменяю статус без удаления');
				db.query("UPDATE `groups_to_wall` SET `status` = '2', type = 'is_marked_as_ads' WHERE vk_group_id = '"+wall['owner_id']+"' AND vk_post_id = '"+wall['id']+"' ");
				return 0;
			}
            if(row[0]['is_marked_as_ads'] == 1 && wall['marked_as_ads'] == 1){
                var vk_remove = parseInt(wall['date'] + (row[0]['hours'] * 60));
                var status    = 0;
                if(row[0]['is_not_remove'] == 1){
                    vk_remove = wall['date'];
                    status    = 2;
                }
                db.query("UPDATE `groups_to_wall` SET `status` = '"+status+"', type = 'is_marked_as_ads', vk_remove = '"+vk_remove+"' WHERE vk_group_id = '"+wall['owner_id']+"' AND vk_post_id = '"+wall['id']+"'");
                if(row[0]['is_not_remove'] == 0){
                    
                }
                return 0;
            }
            

			if(filter.length > 0){
				for(var i = 0; i < filter.length; i++){
					var f = filter[i];

					if(empty(f) == true){
						continue;
					}

					if(string.indexOf(f) + 1){
						addLogByGroup("-"+group['gid'], 'Общий фильтр, нашел в записи : ' + f + ' запускаю проверку по массовому фильтру');
						var dataTime = {
							"time"  : row[0]['hours'],
                            "is_not_remove": row[0]['is_not_remove'],
							"search": f,
							"search_to_string": string
						};
						return getFilterTypeMass(group, wall, type, dataTime, string);
						break;
					}
				}
			}
            var wallRepost = 0;
            if(isset("copy_history", wall)){
                wallRepost = 1;
            }
            //Если удаление всех записей с репостами включена
            if(wallRepost == 1 && is_copy_history == 1){
                return getFilterTypeMass(group, wall, type, {
                    "time"  : row[0]['hours'],
                    "is_not_remove": row[0]['is_not_remove'],
                    "search": "",
                    "search_to_string": string
                }, string);
            }
            //Фильтры ничего не нашли, охват выключен.
            if(group['gc_status'] == 0){
                db.query("UPDATE `groups_to_wall` SET status = '5', type = 'post' WHERE vk_post_id = '" + wall['id'] + "' AND vk_group_id = '" + wall['owner_id'] + "'");
            }
		}
	});
	return false;
}
/*
 *
 * Функция для фильтрации данных стены по фильтру группы
 *
*/
function getGroupFilterForWall(group, wall) {
    console.log('Запускаю анализ записей по фильтрам в группе ' + group['id']);
    //Проверка на закрепленную тему 
    if (wall['is_pinned'] == 1 && group['is_pinned'] == 0) {
        addLogByGroup(wall['owner_id'], 'Нашел закрепленную запись, в настройках группы выключено удаление, пропускаю запись');
        db.query("UPDATE groups_to_wall SET status = '2', type = 'is_pinned' WHERE vk_group_id = '"+wall['owner_id']+"' AND vk_post_id = '"+wall['id']+"'");
        return 0;
    }
    
    //Запускам поиск фильтров
    var stringTotal = "";
        stringTotal+= wall['text'];
    //Проверяем есть ли прикрепленный репост или нет
    if(isset("copy_history", wall)){
        for(var row in wall['copy_history']){
            if(wall['copy_history'][row]['post_type'] == 'post'){
                stringTotal += wall['copy_history'][row]['text'];
            }
        }
    }
    getFilterTypeGeneral(group, wall, 0, stringTotal);

    //Поиск по Посту
    if (isset("attachment", wall)) {
        if (wall['attachment']['type'] == 'link') {
            if (isset("title", wall['attachment']['link'])) {
                getFilterTypeMass(group, wall, 1, {}, wall['attachment']['link']['title']);
            }
        }
    }
}
//Парсинг записей
function wallGet(taskId, group){
    console.log('Запускаю задачу №: ' + taskId + ' для группы '+group['gid']);
    //Парсинг стены группы
    request('https://api.vk.com/method/wall.get?owner_id=-' + group['gid'] + '&offset=0&count=100&access_token=' + group['token'] + '&v=5.78', function(error, response, body) {
        if (typeof response !== "undefined" && response.statusCode == 200) {
            var walls = JSON.parse(body);
            if(isset("response", walls)){
                var storage    = {
                	"response": []
                };
                var storage_id = 1;
                var to_date = Math.round(new Date().getTime() / 1000) - (group['view_post_before'] * 24 * 3600);

                for(var i = 0; i < walls['response']['items'].length; i++){
                	if(walls['response']['items'][i]['date'] >= to_date){
                		storage['response'][storage_id] = walls['response']['items'][i]; 
                		storage_id = storage_id + 1;
                 	}
                }
                var walls = storage;
                console.log('В группе ' + group['gid'] + ' нашел записей '+walls['response'].length);
                if(walls['response'].length == 0){
                    application.clearnWallByGroup(taskId, group);
                }
                
                if(walls['response'].length > 0 && in_array('filter', group['serviceList']) == true && in_array('coverage', group['serviceList']) == false){
                    //console.log('Запускаем обработку по фильтрам');
                    for( var row in walls['response']){
                        addWallForGroup(taskId, group, walls['response'][row], [], {
                            "likes"   : 0,
                            "comments": 0,
                            "reposts" : 0,
                            "lastIndex": walls['response'][parseInt(walls['response'].length -1)]['id'],
                        });
                    }
                }
                if(walls['response'].length > 0 && in_array('coverage', group['serviceList']) == true){
                    //console.log('Запускаем обработку по охвату');
                	addLogByGroup('-'+group['gid'], 'Запуск обработки записей');
                	var indexWall  = 1;
                    //Кол-во записей
                    var totalWallPost = walls['response'].length -1;
                    //Кол-во запросов за 1 раз 
                    var totalQueryForFirst = 25;
                    //Кол-во постов для обработки
                    var totalQueryForFirstThis = 0;
                    //Кол-во заходов
                    var totalUseInterval = Math.ceil(totalWallPost / totalQueryForFirst);
                    //Кол-во обработаных запросов на момент Интервала
                    var totalUsedWallPost = totalWallPost;
                    //Кол-во проходов интервала
                    var totalUsedInterval = 0;
                    
                	var intervalId = setInterval(function(){

                        if(totalUsedInterval == totalUseInterval){
                            addLogByGroup("-"+group['gid'], 'Обработка записей закончена, Записей обработано: ' + (indexWall - 1));
                            clearInterval(intervalId);
                            indexWall = 0;
                        }else{
                            if(totalQueryForFirst <= totalUsedWallPost){
                                totalQueryForFirstThis = 25;                     
                            }else{
                                totalQueryForFirstThis = totalQueryForFirst - (totalQueryForFirst - totalUsedWallPost);
                            }
                            
                            var code = 'return {'; 
                            var codeStats = '';
                            for(var i = 0; i < totalQueryForFirstThis; i++){
                                codeStats   += '"' + indexWall + '" : API.stats.getPostReach({"owner_id": "-'+group['gid']+'", "post_id": "'+walls['response'][indexWall]['id']+'"}), '; 
                                indexWall = indexWall + 1;
                            }
                            code += 'stats: {'+codeStats+'}';
                            code += '};';
                            totalUsedWallPost = totalUsedWallPost - totalQueryForFirstThis;

		                	request('https://api.vk.com/method/execute?code='+code+'&access_token='+group['token']+'&v=3.0', function(error, response, body) {
		                	    if (typeof response !== "undefined" && response.statusCode == 200) {
		                	        var execute = JSON.parse(body);
                                    if(isset("response", execute)){
                                        var execute = execute['response'];
                                        for(var row in execute['stats']){
                                            var vk_likes    = 0;
                                            if(typeof walls['response'][row]['likes']['count'] !== 'undefined'){
                                                vk_likes = walls['response'][row]['likes']['count'];
                                            }
                                            var vk_comments = 0;
                                            if(typeof walls['response'][row]['comments']['count'] !== 'undefined'){
                                                vk_comments = walls['response'][row]['comments']['count'];
                                            }
                                            var vk_reposts  = 0;
                                            if(typeof walls['response'][row]['reposts']['count'] !== 'undefined'){
                                                vk_reposts = walls['response'][row]['reposts']['count'];
                                            }
                                            addWallForGroup(taskId, group, walls['response'][row], execute['stats'][row][0], {
                                                "likes"   : vk_likes,
                                                "comments": vk_comments,
                                                "reposts" : vk_reposts,
                                                "lastIndex": walls['response'][parseInt(walls['response'].length -1)]['id'],
                                            });
                                        }
                                    }else{
                                        var errorCode = execute['error']['error_code'];
                                        applicationTaskMeneger.errorTask(taskId, errorCode, 'checkGroup');
                                    }
		                		}else{
                                    addLogByGroup("ERROR RESPONSE", JSON.stringify(response));
                                }
		                	});
                        }
                        totalUsedInterval = totalUsedInterval +1;
                	}, 1000);
                }
            }else{
            	var errorCode = walls['error']['error_code'];
                applicationTaskMeneger.errorTask(taskId, errorCode, 'checkGroup');
            }
        }else {
           applicationTaskMeneger.errorTask(taskId, 's1', 'checkGroup');
        }
    });
}

//Добавление группы в базу данных
function addWallForGroup(taskId, group, wall, statistica, storage){
	//Определяем статус записи
	var status = 0;
    var field_status = '';
    var vk_coverage      = 0,
        vk_reach_viral   = 0,
        vk_reach_ads     = 0, 
        vk_reach_total   = 0,
        vk_links         = 0,
        vk_to_group      = 0,
        vk_join_group    = 0,
        vk_report        = 0,
        vk_hide          = 0,
        vk_unsubscribe   = 0,
        is_pinned        = 0,
        vk_created_by    = 0,
        gtw_type         = '';
        
    vk_created_by = wall['created_by'];
       
    if(typeof statistica !== "undefined"){
        if(typeof statistica['reach_subscribers'] !== "undefined") vk_coverage = statistica['reach_subscribers'];
        if(typeof statistica['reach_viral'] !== "undefined")       vk_reach_viral = statistica['reach_viral'];
        if(typeof statistica['reach_ads'] !== "undefined")         vk_reach_ads = statistica['reach_ads'];
        if(typeof statistica['reach_total'] !== "undefined")       vk_reach_total = statistica['reach_total'];
        if(typeof statistica['links'] !== "undefined")             vk_links = statistica['links'];
        if(typeof statistica['to_group'] !== "undefined")          vk_to_group = statistica['to_group'];
        if(typeof statistica['join_group'] !== "undefined")        vk_join_group = statistica['join_group'];
        if(typeof statistica['report'] !== "undefined")            vk_report = statistica['report'];
        if(typeof statistica['hide'] !== "undefined")              vk_hide = statistica['hide'];
        if(typeof statistica['unsubscribe'] !== "undefined")       vk_unsubscribe = statistica['unsubscribe'];
    }
    if(isset('is_pinned', wall)){
        is_pinned = 1;
        console.log('Нашел закрепленую запись в группе:'+group['gid'] + ', запись №'+wall['id']);
        if(group['is_pinned'] == 0 && wall['is_pinned'] == 1){ 
            status = 2; 
            field_status = ", status = '2', type = 'is_pinned'";
            gtw_type = 'is_pinned';
        }
    }
    
	db.query("SELECT COUNT(*) as total FROM `groups_to_wall` WHERE group_id = '" +group['id']+ "' AND vk_post_id = '" +wall['id']+ "'", function(error, total){
		if(error) throw error;
        if(total[0]['total'] == 0){
            //Добавляем новую запись
        	addLogByGroup(wall['owner_id'], 'Добавляю новую запись: '+ wall['id']);
        	var vk_remove = parseInt(wall['date']) + parseInt(group['time'] * 60);
        	var date_remove = '0000-00-00 00:00:00';

        	db.query("INSERT INTO `groups_to_wall`(`type`, `group_id`, `vk_group_id`, `vk_post_id`, `vk_created_by`, `vk_date`, `vk_remove`, `date_add`, `date_update`, `date_remove`, `status`) VALUES ('"+gtw_type+"', '"+group['id']+"', '"+wall['owner_id']+"', '"+wall['id']+"', '"+vk_created_by+"', '"+wall['date']+"', '"+vk_remove+"', NOW(), NOW(), '"+date_remove+"', '"+status+"')", function(error, result){
                
                if(error) return console.log(error);
                
                //Добавление статистики
                db.query("INSERT INTO `groups_to_wall_stat`(`vk_group_id`, `vk_post_id`, `vk_coverage`, `vk_reach_viral`, `vk_reach_ads`, `vk_reach_total`, `vk_links`, `vk_to_group`, `vk_join_group`, `vk_report`, `vk_hide`, `vk_unsubscribe`, `vk_likes`, `vk_comments`, `vk_reposts`) VALUES ('"+wall['owner_id']+"', '"+wall['id']+"', '"+vk_coverage+"', '"+vk_reach_viral+"', '"+vk_reach_ads+"', '"+vk_reach_total+"', '"+vk_links+"', '"+vk_to_group+"', '"+vk_join_group+"', '"+vk_report+"', '"+vk_hide+"', '"+vk_unsubscribe+"', '"+storage['likes']+"', '"+storage['comments']+"', '"+storage['reposts']+"')");
                
        		//Запускаем парсинг записи
                VP.loadPreviewZip(wall['owner_id'], wall['id']);
                //Если услуга фильтры включена, отправляем запись на проверку по фильтрам
                if(in_array('filter', group['serviceList']) == true){
                    getGroupFilterForWall(group, wall);
                }
        	});
        }else{
        	addLogByGroup(wall['owner_id'], 'Обновляю данные записи: '+ wall['id']);
        	//Обновляем запись
        	db.query("UPDATE `groups_to_wall` SET `date_update` = NOW() "+field_status+" WHERE `group_id` = '"+group['id']+"' AND `vk_post_id` = '"+wall['id']+"' AND `status` != '2'");
            //Обновление статистики записи
            db.query("UPDATE `groups_to_wall_stat` SET `vk_coverage` = '"+vk_coverage+"', `vk_reach_viral` = '"+vk_reach_viral+"', `vk_reach_ads` = '"+vk_reach_ads+"', `vk_reach_total` = '"+vk_reach_total+"', `vk_links` = '"+vk_links+"', `vk_to_group` = '"+vk_to_group+"', `vk_join_group` = '"+vk_join_group+"', `vk_reposts` = '"+storage['reposts']+"', `vk_report` = '"+vk_report+"', `vk_hide` = '"+vk_hide+"', `vk_unsubscribe` = '"+vk_unsubscribe+"', `vk_likes` = '"+storage['likes']+"', `vk_comments` = '"+storage['comments']+"' WHERE vk_group_id = '"+wall['owner_id']+"' AND vk_post_id = '"+wall['id']+"'");
            
            //Если запись закреплена, убираем метки у других записей
            if(is_pinned == 1){
                db.query("DELETE FROM `groups_to_wall` WHERE vk_group_id = '"+wall['owner_id']+"' AND vk_post_id != '"+wall['id']+"' AND type = 'is_pinned' AND status != '1'")
            }
        }
	});
    //Проверка на последнюю запись
    if(storage['lastIndex'] == wall['id']){
        //Очистка стены
        //Завершение задачи по анализу стены
        applicationTaskMeneger.successTask(taskId, 2);
        application.clearnWallByGroup(taskId, group);
    }
}
function in_array(value, data){
    for(var i = 0; i < data.length; i++){
        if(data[i] == value) return true;
    }
    return false;
}
function isset(key, data) {
    if(key in data){
    	return true
    }else{
    	return false;
    }
}
function empty(str){
	if (str !== null && str.trim() == ''){
		return true;
	}else{
		return false;
	}
}